﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class MongoDbInitializer : IDbInitializer
    {
        private IMongoCollection<Role> _roleCollection;
        private IMongoCollection<Employee> _employeeCollection;

        public MongoDbInitializer(
            IMongoCollection<Role> roleCollection,
            IMongoCollection<Employee> employeeCollection)
        {
            _roleCollection = roleCollection;
            _employeeCollection = employeeCollection;
        }

        public void InitializeDb()
        {

            _roleCollection.DeleteMany(_ => true);
            _roleCollection.InsertMany(FakeDataFactory.Roles);

            _employeeCollection.DeleteMany(_ => true);
            _employeeCollection.InsertMany(FakeDataFactory.Employees);

        }
    }
}
