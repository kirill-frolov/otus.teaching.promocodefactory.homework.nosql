﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class MongoDbInitializer : IDbInitializer
    {
        private IMongoCollection<Preference> _preferenceCollection;
        private IMongoCollection<Customer> _customerCollection;
        public MongoDbInitializer(
            IMongoCollection<Preference> preferenceCollection,
            IMongoCollection<Customer> customerCollection)
        {
            _preferenceCollection = preferenceCollection;
            _customerCollection = customerCollection;
        }

        public void InitializeDb()
        {
            _preferenceCollection.DeleteMany(_ => true);
            _preferenceCollection.InsertMany(FakeDataFactory.Preferences);

            _customerCollection.DeleteMany(_ => true);
            _customerCollection.InsertMany(FakeDataFactory.Customers);
        }
    }
}
